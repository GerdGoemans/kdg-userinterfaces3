dark mode:
https://quasar.dev/quasar-plugins/dark

Translation:
http://kazupon.github.io/vue-i18n/installation.html
https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-vue-app-with-vue-i18n

Cookies:
https://quasar.dev/quasar-plugins/cookies

Alert:
https://constkhi.github.io/vue-simple-alert/

layout:
https://v0-17.quasar-framework.org/components/typography.html

Icons;
https://material.io/resources/icons/?style=baseline

input:
https://quasar.dev/vue-components/input