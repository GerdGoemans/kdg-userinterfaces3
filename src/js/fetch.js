/* eslint no-throw-literal: 0 */
const APIURL = "http://localhost:3000";

async function fetchBase(URLSuffix = "", params = {}) {
	return fetch(APIURL + URLSuffix, params);
}

async function fetchDataBase(URLSuffix = "") {
	try {
		if (URLSuffix === "") throw "URL suffix cannot be empty";
		let response = await fetchBase(URLSuffix);
		if (response.ok) {
			return await response.json();
		}
		throw response;
	} catch (e) {
		throw e;
	}
}

async function fetchDepartments() {
	return fetchDataBase("/departments");
}

async function fetchRooms() {
	return fetchDataBase("/rooms");
}

async function fetchActions() {
	return fetchDataBase("/actions");
}

async function fetchFacilities() {
	return fetchDataBase("/facilities");
}

async function fetchFacilitie(id = "") {
	if (id === "") throw "Id cannot be empty";
	let response = await fetchDataBase("/facilities/" + id);
	return response;
}

async function fetchRoles() {
	return fetchDataBase("/roles");
}

async function fetchUsers() {
	return fetchDataBase("/users");
}

async function fetchUserById(id = "") {
	if (id === "") throw "User Id cannot be empty";
	return fetchDataBase("/users/" + id);
}

async function fetchUserByToken(token = "") {
	let currentToken;
	if (token === "") {
		throw "Token cannot be empty";
	} else {
		currentToken = token;
	}
	try {
		let users = await fetchUsers();

		for (let i = 0; i < users.length; i++) {
			const user = users[i];

			for (let j = 0; j < user.activeTokens.length; j++) {
				const tk = user.activeTokens[j];
				if (tk.token === currentToken) {
					return user;
				}
			}
		}
		throw "User not found";
	} catch (e) { throw e };
}

async function isAdminUser(token = "") {
	let user = await fetchUserByToken(token);
	return user.roles.includes('role000');
}

async function isDoctor(token = "") {
	let user = await fetchUserByToken(token);
	return user.roles.includes('role003');
}

async function userNameExists(name = "") {
	try {
		if (name === "") throw "name cannot be empty";
		let response = await fetchDataBase("/users/" + name);
		return response.status !== 404;
	} catch (e) {
		return e.status !== 404;
	}
}

async function putPostBase(URLSuffix = "", data, type = "POST") {
	return fetchBase(URLSuffix, {
		method: type,
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json"
		},
		body: JSON.stringify(data)
	});
}

async function putUser(user = "") {
	if (user === "") throw "User must be given";
	try {
		let response = await putPostBase("/users/" + user.id, user, "PUT");
		if (response.ok) {
			return true;
		}
		throw response;
	} catch (e) { throw e; };
}

async function putPatient(patient = "") {
	if (patient === "") throw "Patient must be given";
	try {
		let response = await putPostBase("/patients/" + patient.id, patient, "PUT");
		if (response.ok) {
			return true;
		}
		throw response;
	} catch (e) { throw e; };
}

async function postUser(user = "") {
	if (user === "") throw "User must be given";
	try {
		let response = await putPostBase("/users", user, "POST");
		if (response.ok) {
			return true;
		}
		throw response;
	} catch (e) { throw e; };
}

async function postPatient(patient = "") {
	if (patient === "") throw "User must be given";
	try {
		let response = await putPostBase("/patients", patient, "POST");
		if (response.ok) {
			return true;
		}
		throw response;
	} catch (e) { throw e; };
}

async function removeUserToken(token = "") {
	if (token === "") throw "Token cannot be empty";
	try {
		let user = await fetchUserByToken(token);
		if (user === null) throw new Error("Couldn't find user with specified token");
		user["activeTokens"].pop(token);
		let success = await putUser(user);
		return success;
	} catch (e) {
		throw e
	};
}

async function fetchPatientById(token = "", id = "") {
	if (id === "") throw "Id cannot be empty";
	if (token === "") return {};
	return fetchDataBase("/patients/" + id);
}

async function loadRoomData(token = "", roomId = -1) {
	if (roomId === -1) throw "Room Id must be passed";
	try {
		let user = { "roles": ["-1"] };
		try {
			user = await fetchUserByToken(token);
		} catch (e) { /* ignore, user may receive data even when not logged in */ }

		const result = {};
		// Load rooms
		let room = await fetchDataBase("/rooms/" + roomId);
		Object.keys(room).forEach(key => (result[key] = room[key]));

		// Load Patient in said room
		if (room.patientId) { // <= to ensure that patientId exist (for empty rooms)
			try {
				let patient = await fetchPatientById(token, room.patientId);
				Object.keys(patient).forEach(key => {
					if (key !== "id") return (result[key] = patient[key]);
				});
			} catch (e) {
				/* patientId might not exist but room data should be returned regardless */
				// todo: remove invalid patient Ids
			}
		}
		// sanitize output for user permissions
		const sanitizeResult = {};
		Object.keys(result).forEach(key => {
			switch (key) {
				case "x":
				case "y":
				case "height":
				case "width":
				case "roomId":
				case "patientPicture":
				case "calledForHelp":
				case "dateIntake":
				case "reasonIntake":
				case "redenOpname":
				case "depId": {
					return (sanitizeResult[key] = result[key]);
				}
				case "doctorId":
				case "heartRate":
				case "bp_upper":
				case "bp_lower":
				case "lastname":
				case "saturation":
				case "vegi":
				case "vegan":
				case "actions":
				case "firstname":
				case "facilities":
				case "patientId": {
					if (user.roles.includes("role002") || user.roles.includes("role003")) {
						return (sanitizeResult[key] = result[key]);
					}
					break;
				}
				case "id": {
					// ignore
					break;
				}
				default:
					throw "Error, key " + key + " not recognized";
				// return (sanitizeResult[key] = result[key]);
			};
		});
		return sanitizeResult;
	} catch (e) { throw e; }
}

function getAPIURL() {
	return APIURL;
}

const fetchFn = {
	getAPIURL: getAPIURL,
	get: fetchBase, // returns response
	getDepartments: fetchDepartments, // returns list of departments
	getRoles: fetchRoles, // returns list of roles
	getRooms: fetchRooms, // returns list of rooms
	getActions: fetchActions, // returns list of actions
	getFacilities: fetchFacilities, // returns list of facilities
	getFacilitie: fetchFacilitie, // returns a facilitie based on the given id
	getPatientById: fetchPatientById, // returns a patient based on the given id
	getUsers: fetchUsers, // returns users
	getUserById: fetchUserById, // returns user
	getUserByToken: fetchUserByToken, // returns user
	userNameExist: userNameExists, // returns boolean (true for user exist, false for user doesn't exist)
	isAdminUser: isAdminUser, // returns true if a user is an admin, false if not
	isDoctor: isDoctor, // returns true if a user is an admin, false if not
	loadRoomData: loadRoomData, // returns room data based on user privilige
	put: putPostBase, // returns response
	putUser: putUser, // returns true on success, throws exception on fail
	postUser: postUser, // returns true on success, throws exception on fail
	postPatient: postPatient, // returns true on success, throws exception on fail
	putPatient: putPatient, // returns true on success, throws exception on fail
	deleteUserToken: removeUserToken // returns boolean (true for successful removal of token)
};

export default fetchFn;
